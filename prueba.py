#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

filename = "C:\\Users\\DEIBIS\\Documents\\ReporteAsistencia20220106151645.xls"
# Obtener el tamaño del archivo a enviar.
filesize = os.path.getsize(filename)
# Informar primero al servidor la cantidad
# de bytes que serán enviados.
print(filesize)
# Enviar el archivo en bloques de 1024 bytes.
texto1 = ""
texto2 = ""
with open(filename, "rb") as f:
    while True:
        read_bytes = f.read(1024*1024)
        if read_bytes == "":
            # print(read_bytes)
            break
        texto1 = read_bytes+texto1
        print(read_bytes)

print("Ahora la copia")

with open(filename, "rb") as f:
    texto2 = f.read()
    print(texto2)

if texto2 == texto1:
    print("iguales")
